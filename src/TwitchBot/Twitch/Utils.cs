﻿using System;

namespace TwitchBotBase.Twitch
{
    public static class Utils
    {
        public const string TwitchTmi = "tmi.twitch.tv";
        public const string UnknownUser = "UNKNWN_USR";
        private const int UsernameMaxLength = 25;

        // why
        /// <summary>
        /// Parses out the username from the given prefix data.
        /// </summary>
        /// <param name="prefix">The prefix data.</param>
        /// <returns>The username. If parsing failed it wil return "UNKNWN_USR"</returns>
        public static string GetUsername(string prefix)
        {
            if (prefix.StartsWith(TwitchTmi))
                return TwitchTmi;

            if (prefix.IndexOf('!') <= UsernameMaxLength && prefix.IndexOf('!') > 0)
                return prefix.Substring(0, prefix.IndexOf('!'));

            if (prefix.Contains(TwitchTmi))
                return prefix.Substring(0, prefix.IndexOf(TwitchTmi, StringComparison.Ordinal) - 1);

            return UnknownUser;
        }
    }
}
