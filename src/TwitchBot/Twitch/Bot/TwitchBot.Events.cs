﻿using System;

namespace TwitchBotBase.Twitch.Bot
{
    public partial class TwitchBot
    {
        public event EventHandler<EventArgs> DisconnectFromTwitch = delegate { }; 
        public event EventHandler<MessageReceivedEventArgs> AnyMessageReceived = delegate { };
        public event EventHandler<MessageReceivedEventArgs> ChatMessageReceived = delegate { };
        public event EventHandler<ChannelJoinedEventArgs> ChannelJoined = delegate { };
        public event EventHandler<ChannelLeftEventArgs> ChannelLeave = delegate { };

        private void OnChannelJoined(string channelName)
           => ChannelJoined(this, new ChannelJoinedEventArgs(channelName));

        public void OnChannelLeave(string channelName)
            => ChannelLeave(this, new ChannelLeftEventArgs(channelName));

        private void OnChatMessageReceived(string raw, IrcMessage irc)
            => ChatMessageReceived(this, new MessageReceivedEventArgs(raw, irc));

        private void OnAnyMessageReceived(string raw, IrcMessage irc)
            => AnyMessageReceived(this, new MessageReceivedEventArgs(raw, irc));

        private void OnDisconnectFromTwitch()
            => DisconnectFromTwitch(this, EventArgs.Empty);
    }

    public sealed class ChannelLeftEventArgs : EventArgs
    {
        public string Channel { get; }

        public ChannelLeftEventArgs(string channel)
        {
            Channel = channel;
        }
    }

    public sealed class ChannelJoinedEventArgs : EventArgs
    {
        public string Channel { get; }

        public ChannelJoinedEventArgs(string channel)
        {
            Channel = channel;
        }
    }

    public sealed class MessageReceivedEventArgs : EventArgs
    {
		public IrcMessage Message { get; }
		public string Raw { get; }

        public MessageReceivedEventArgs(string raw, IrcMessage irc)
        {
            Message = irc;
            Raw = raw;
        }
    }
}
