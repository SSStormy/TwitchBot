﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace TwitchBotBase.Twitch.Bot
{
    public partial class TwitchBot : IDisposable
    {
        private readonly ManualResetEvent _disconnectEvent = new ManualResetEvent(false);

        private TcpClient _client;
        private NetworkStream _stream;
        private StreamWriter _writer;
        private StreamReader _reader;

        private bool _shouldClose;

        private readonly List<string> _connectedChannels = new List<string>();

        ///<summary>Returns a list of channels the bot is currently connected to.</summary>
        public IEnumerable<string> Channels => _connectedChannels.AsReadOnly();

        public bool IsConnected { get; private set; }
        public int Port { get; }

        public TwitchBot(int port = 6667)
        {
            if (port <= 0) throw new ArgumentException(nameof(port));

            Port = port;
        }

        public Task Connect(string username, string oauth)
            => Task.Run(() =>
            {
                if (string.IsNullOrEmpty(username)) throw new ArgumentNullException(nameof(username));
                if (string.IsNullOrEmpty(oauth)) throw new ArgumentNullException(nameof(oauth));

                _disconnectEvent.Reset();

                _client = new TcpClient("irc.twitch.tv", Port);

                _stream = _client.GetStream();
                _writer = new StreamWriter(_stream) {AutoFlush = true};
                _reader = new StreamReader(_stream);

                Send("PASS " + oauth);
                Send("NICK " + username);

                _shouldClose = false;

                Task.Run(async () => await ReadData());
            });

        ///<summary>Disconnects the client from the irc server.</summary>
        public void Disconnect()
        {
            _client.Close();
            _stream.Close();
            _writer.Close();
            _reader.Close();

            _shouldClose = true;
        }

        /// <summary>Receives raw messages from twitch chat servers and sends them to HandleMessage.</summary>
        private async Task ReadData()
        {
            while (!_shouldClose)
            {
                if (_stream.DataAvailable)
                {

                    string dataIn = _reader.ReadLine();

                    if (dataIn != null && dataIn.StartsWith("PING"))
                        Send("PONG :tmi.twitch.tv");

                    IrcMessage ircMsg = new IrcMessage(dataIn);

                    OnAnyMessageReceived(dataIn, ircMsg);

                    if (ircMsg.Username != Utils.TwitchTmi && // filter out junk messages
                        ircMsg.Channel.StartsWith("#") &&
                        !string.IsNullOrEmpty(ircMsg.Text))
                        OnChatMessageReceived(dataIn, ircMsg);
                }
                await Task.Delay(1);
            }
            IsConnected = false;
            OnDisconnectFromTwitch();
            _disconnectEvent.Set();
        }

        ///<summary> Blocking call and wait until the client has disconnected.</summary>
        public void Wait()
            => _disconnectEvent.WaitOne();

        /// <summary> Makes sure that the channel has a # at the start.</summary>
        public static string NormalizeChannelName(string channel)
        {
            if (channel.StartsWith("#"))
                return channel;
            return "#" + channel;
        }

        /// <summary>Joins a given channel.</summary>
        public void JoinChannel(string channel)
        {
            channel = NormalizeChannelName(channel);

            if (_connectedChannels.Contains(channel)) return;

            _writer.WriteLine("JOIN " + channel);
            _connectedChannels.Add(channel);

            OnChannelJoined(channel);
        }

        /// <summary> Disconnects from a given channel.</summary>
        public void PartChannel(string channel)
        {
            channel = NormalizeChannelName(channel);

            if (!_connectedChannels.Contains(channel)) return;

            _writer.WriteLine("PART " + channel);
            _connectedChannels.Remove(channel);

            OnChannelLeave(channel);
        }

        /// <summary>Sends a raw IRC message to the twitch servers. </summary>
        public void Send(string message)
            => _writer.WriteLine(message);

        /// <summary>Sends a message to the given channel.</summary>
        public void SendMessage(string channel, string message)
            => _writer.WriteLine("PRIVMSG " + channel + " :" + message);

        /// <summary>Disposes the bot.</summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_client != null)
                {
                    _client.Close();
                    _client = null;
                }
                if (_stream != null)
                {
                    _stream.Dispose();
                    _stream = null;
                }
                if (_writer != null)
                {
                    _writer.Dispose();
                    _writer = null;
                }
                if (_reader != null)
                {
                    _reader.Dispose();
                    _reader = null;
                }
            }
        }
    }
}
