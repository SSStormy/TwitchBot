﻿using System.Text.RegularExpressions;

namespace TwitchBotBase.Twitch
{
    public class IrcMessage
    {
        private const int PrefixIndex = 1;
        private const int ReceiverIndex = 3;
        private const int ParametersIndex = 4;

        public string Prefix { get; }
        public string Username { get; }
        public string Channel { get; }
        public string Text { get; }

        /// <summary>
        /// Constructs a new irc message based upon the raw data from the irc server.
        /// </summary>
        /// <param name="raw"></param>
        public IrcMessage(string raw)
        {
            Regex parsing = new Regex(@"^(?::(\S+) )?(\S+)(?: (?!:)(.+?))?(?: :(.+))?$", RegexOptions.Multiline);
            Match messageMatch = parsing.Match(raw);

            if (!messageMatch.Success) return;

            Prefix = messageMatch.Groups[PrefixIndex].Value; //Prefix
            Username = Utils.GetUsername(Prefix);
            Channel = messageMatch.Groups[ReceiverIndex].Value; //Receiver
            Text = messageMatch.Groups[ParametersIndex].Value; //Params
        }
    }
}
